# Demands of different parties

To hold an overview, is the focus first on camp planning and later course planning. Course planning must be way more formal and has stricter guidelines.

## PBS - Project DigiScout
- **eCamp Languages:** support (at least) German, French, Italian
- **Course-planning:** optimizing course-planning functionality
- **Connect camp planning and automatize camp administration:** extend eCamp for connection to camp planning and camp administration papers and automatisms (i.e. option to automatically send forms to PBS and cantonal secretariat). Integration with MiData (i.e. selection of camp leader team, open a new camp from MiData)
- **Connect course planning and automatize course administration:** forms for courses can automatically be sent to PBS and cantonal secretariat
- collecting data from planning to create statistics which will help to set focus points in PBS and cantonal associations. This data collection should not increase load of work for user.


## User camp planning tool

- has to be responsive. It is often opened on mobile or smaller devices.
- give us the option to open several sessions or a function to compare directly with old camps (copy-paste makes live easier)
- short-cuts/short-keys to speed up the usage (low priority, because only advanced user would use it)
- integration of forms from third parties - simplify and centralize process
- open a camp in MiData automatically opens a camp in eCamp
- the provided map is completely useless (in case it should work for once)
- add additional (third party) PDFs to complete PDF -- not sure what he meant, will check this

## User course planning tool

- course planning has more demands than camp planning (concrete list coming soon)
    *
- integration of forms from third parties - simplify and centralize process

## Coaches/Supervisor

- simplify supervision of camps/courses to decrease load of work
- uniform documents in all steps would be great
- coach should be able to change percentage of blocks
