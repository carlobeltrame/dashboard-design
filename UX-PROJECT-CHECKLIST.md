# UX/UI Project Checklist of eCamp

## Research

- [x] Analyze current UX/IA structure -- Create an overview of the project as is
- [ ] Competitive Analysis -- See how others solve similar problems and compare
- [ ] Data analysis -- Have a look at funnels, clicks, page views, performance, etc. 
- [ ] User feedback -- What do the actual user say? What do they actually do?

## Plan

- [ ] User stories -- create personas (fictional users) write down user stories and scenarios
- [ ] User flows -- create user's flow based on the scenarios created. This is used later to review the journey and create wireframes on top of each step
- [ ] Red routes -- define red routes (critical design paths) for the project. This helps identify, prioritise, and eliminate any usability obstacles on key user journeys.

## Explore

- [ ] Brainstorm and sketch -- sit together and discuss the results form the previous steps
- [ ] Wireframe -- add some details and structure to your ideas, reuse patterns and create pages on top of your user flows
- [ ] Prototype -- start creating paper prototypes and continuously iterate to more functional ones. Get some people to test them. 
- [ ] Find branding pattern -- find a unique guideline for the design, create a color scheme and a basic design priciples. Give some life to the UX prototypes. 

## Communicate

- [ ] Information Architecture -- Understand your users, your data structure and your channels. How can you organsie your navigation and content in a clear and consistent way?
- [ ] Language -- follow your brand personality, keep in mind users' culture and language, the context of your product and make sure they understand you
- [ ] Accesibility -- you don't need to add extra functionality or to duplicate any content. The key is simply to assess the requirements of those with differnt skills and limited devices

## Create

- [ ] UI elements -- reuse elements and patterns. Follow your style guidelines. Start small, then create pages
- [ ] Gestures -- Where should the user act in which way? Does the user have to pinch, drag, zoom, rotate,... 
- [ ] Responsiveness -- Is the project usable on any device where is should?


## Give feedback

- [ ] Waiting times -- Indicate that something is going on when the user has to wait. If the waiting time is long, may try something entertaining as loading screen.
- [ ] Errors -- Be clear and specific no what and where user's error is. If it's the projects fault that there was an error, say so.
- [ ] Completed actions -- give immediate and clear feedback of successful user's actions. Do not always wait for server response, trust your server once in a while!

## Finalise

- [ ] Finalise layout -- don't stop with the first design, always ask "is this the best you can do?"
- [ ] Use of images and icons -- use of icons and images is strongly influenced by context, culture and layout that you use. Like icons, test your images, small changes can bring huge improvments. It has to be clear what you want to say with the icon/image
- [ ] Font and colors hierachy -- use colors and font sizes properly, try to follow your guidelines and keep it simple. The best visual hierachies lead users to take the action confidently.

## Delight

- [ ] Mirco copy -- every word is important, and a bit of personality help your project
- [ ] Micro interactions -- trigger, rules, feedback, loop. Details make the product (maybe some easter eggs)
- [ ] Transitions -- give your design life. Motion shouldn't be only beautiful, it should build menaing about the spatial relationships, functionality, and intention of the system

## Analyse
This part here is after the release (beta or full release). It is mainly statisticcs to improve with later updates.

- [ ] KPI setup - (Key Performance Indicator) -- what do we want to achieve? What are our goals? Write down how we define success and failure and check if we have everything we need to collect the data.
- [ ] A/B Test plan - randomized experiment with two variants (statistics) -- Plan the A/B test ahead and, if you can, plan a short roadmap of improvments. Your goal is not to just improving KIPs, but learning something.
- [ ] Test -- UX lab, survey, sessions recording. The project's first version is running now we can test the UX to it's heart...test, observe and fix, test, observe and fix, and so on... 
